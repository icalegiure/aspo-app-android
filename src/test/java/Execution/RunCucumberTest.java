package Execution;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/java/Features",
		glue = "Steps",
		monochrome=true,
		plugin = {
				"pretty",
				"html:target/cucumber",
				"json:target/cucumber.json"/*,
				"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:"*/
				// si ponemos esta linea, lo cual seria lo correcto, se genera un error
				// por el momento dejamos la linea comentada
				
			}
		)

public class RunCucumberTest {

}
