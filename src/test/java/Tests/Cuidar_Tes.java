package Tests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


import java.net.MalformedURLException;
import java.net.URL;

import org.junit.*;
import org.openqa.selenium.remote.DesiredCapabilities;

import Actions.Driver;
import Pages.*;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;


public class Cuidar_Tes {/*

	AndroidDriver driver;
	
	
	@Before
	public void setUpPrimeraVez() {
		
		driver =  Driver.getDriver();
		/*
		URL url = null;
		
		String u = "http://127.0.0.1:4723/wd/hub";
		 try {
	        	url = new URL(u);
	        }
	        catch (MalformedURLException e) {
	            e.printStackTrace();
	        }
	
		DesiredCapabilities appiumOptions = new DesiredCapabilities();
        appiumOptions.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        appiumOptions.setCapability(MobileCapabilityType.DEVICE_NAME, "MotoE2");       
        appiumOptions.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
        appiumOptions.setCapability("appPackage", "ar.gob.coronavirus");
        appiumOptions.setCapability("appWaitPackage", "ar.gob.coronavirus");
        appiumOptions.setCapability("appActivity", "com.globant.pasaportesanitario.flujos.inicio.InicioActivity");
        appiumOptions.setCapability("appWaitActivity", "com.globant.pasaportesanitario.flujos.inicio.InicioActivity");
        appiumOptions.setCapability("automationName", "UiAutomator1");      
    	driver = new AndroidDriver (url,appiumOptions);  *//*
	}
	
	
	
	@Test
	public void NuevoAutodiagnostico() {			    	
    	try {
    		PaginaIngreso login = new PaginaIngreso(driver);    	
    		
    		login.IngresoDNI("23846270","253284273", "F");
    		
    		PaginaCertificado certificado = new PaginaCertificado(driver);
    		driver.scrollTo("NUEVO");
    		certificado.nuevoAutodiagnostico.click();		
    		
    		PaginaAutodiagnostico  autodiagnostico = new PaginaAutodiagnostico(driver);		
    		autodiagnostico.completarTemperatura("37");

        	PaginaCuestionario cuestionario = new PaginaCuestionario(driver);
        	cuestionario.CompletarCuestionarioNO();
        	
        	PaginaCondicionPreex condicion = new PaginaCondicionPreex(driver);
        	condicion.AvanzarSinEnfermedad();
        	
        	driver.scrollTo("CERTIFICADO");    	
        	boolean verificacion =  certificado.quedateEnCasa.getText().contains("Quedate en casa");
        	
        	driver.scrollTo("NUEVO"); 
        	certificado.desvincular.click();
        	
        	assertTrue("No se visualiza el resultado de diagnostico OK",verificacion);    	  
    	}
    	catch(Exception e )   	{    	
    		System.out.println(e.getMessage());
    		Assert.fail(e.getMessage());
    		}
    	
    	
	}
	
	
	@Test
	public void VerificarCertificadoDeCirculacion() {			    	
    	try {
    		PaginaIngreso login = new PaginaIngreso(driver);    	    		
    		login.IngresoDNI("31773567","492411371", "M");		
    		
    		PaginaCertificado certificado = new PaginaCertificado(driver);  		
        	assertTrue("No se visualiza el resultado de diagnostico OK",certificado.VerificarCertificadoDeCirculacion());    	  
    	}
    	catch(Exception e )   	{    	
    		System.out.println(e.getMessage());
    		Assert.fail(e.getMessage());
    		}
    	
    	
	}
	
	@Test
	public void NuevoUsuario() throws Exception {
	
		
		try {
			
			PaginaIngreso login = new PaginaIngreso(driver);    	
			login.IngresoDNI("33737410","615084894", "M");		
			//login.IngresoDNI("23846270","253284273", "F");
			
			
			PaginaDireccion direccion = new PaginaDireccion(driver);
			direccion.CargarDireccion("Buenos Aires");
			
			PaginaAutodiagnostico  autodiagnostico = new PaginaAutodiagnostico(driver);		
    		autodiagnostico.completarTemperatura("37");

        	PaginaCuestionario cuestionario = new PaginaCuestionario(driver);
        	cuestionario.CompletarCuestionarioNO();
        	
        	PaginaCondicionPreex condicion = new PaginaCondicionPreex(driver);
        	condicion.AvanzarSinEnfermedad();      	
        	        	
        	PaginaCertificado certificado = new PaginaCertificado(driver);
        	certificado.AceptarResultado();
        	
        	driver.scrollTo("CERTIFICADO");    	
        	boolean verificacion =  certificado.quedateEnCasa.getText().contains("Quedate en casa");
        	
        	driver.scrollTo("NUEVO"); 
        	certificado.desvincular.click();
        	
         	assertTrue("No se visualiza el resultado de diagnostico OK",verificacion);    	  
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		
	}
	
	
	@Test
	public void RealizarAutodiagnostico() throws Exception {
	
		
		try {
			
			PaginaIngreso login = new PaginaIngreso(driver)	;	
			login.IngresoDNI("39958866","566910229", "M");
			
			PaginaAutodiagnostico  autodiagnostico = new PaginaAutodiagnostico(driver);		
    		autodiagnostico.completarTemperatura("37");

        	PaginaCuestionario cuestionario = new PaginaCuestionario(driver);
        	cuestionario.CompletarCuestionarioNO();
        	
        	PaginaCondicionPreex condicion = new PaginaCondicionPreex(driver);
        	condicion.AvanzarSinEnfermedad();      	
        	        	
        	PaginaCertificado certificado = new PaginaCertificado(driver);
        	certificado.AceptarResultado();
        	
        	driver.scrollTo("CERTIFICADO");    	
        	boolean verificacion =  certificado.quedateEnCasa.getText().contains("Quedate en casa");
        	
        	driver.scrollTo("NUEVO"); 
        	certificado.desvincular.click();
        	
         	assertTrue("No se visualiza el resultado de diagnostico OK",verificacion);    	  
			
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
		
		
	}
	@After
	public void FinDePruebas(){
		driver.quit();   
	}*/
}
