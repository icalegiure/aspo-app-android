Feature: Realizar el primer uso de un ciudadano autodiagnostico derivado a salud


Scenario Outline: Realizar el primer uso
		Given que abrimos la aplicacion
    When ingresamos dni "<dni>" numero de tramite "<nroTramite>" y genero "<genero>" y presionamos Ingresar
    Then Se loguea correctamente
    Then ingresamos el telefono y la direccion en "<provincia>"
    Then colocamos la temperatura del ciudadano con fiebre 
    And contestamos el cuestionario con peridida de olfato
    And avanzar sin enfermedades confirmando el telefono
    Then el autodianostico Ok se verifica el certificado derivado a salud
   
Examples:
    | dni   | nroTramite | genero | provincia |
    | 42885596 | 345292427 | M | Buenos Aires |