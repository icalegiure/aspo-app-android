package Actions;

import java.util.Properties;
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class Driver {
	
	private static AndroidDriver driver;
	private Properties properties;
	private PropertyLoader propertyLoader;
	private DesiredCapabilities capabilities;
	
	private void CargarProperties() {
		try {
			propertyLoader = new PropertyLoader();
			properties = propertyLoader.getPropValues("configuracion.properties");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	private Driver(boolean cargarCapabilties) {
		CargarProperties();
		CargarCapabilities(cargarCapabilties);
	    try {
	    	driver = new AndroidDriver (new URL(properties.getProperty("url")),capabilities);
	    	//driver = new AndroidDriver (new URL("http://127.0.0.1:4723/wd/hub"),capabilities); 
	    }
	    catch (MalformedURLException e) {
	        e.printStackTrace();
	    }
	}
	
	private void CargarCapabilities(boolean cargar) {
		capabilities = new DesiredCapabilities();
		
		if (cargar) {
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,properties.getProperty("plataforma"));
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, properties.getProperty("nombreEquipo"));       
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
			capabilities.setCapability("appPackage",properties.getProperty("package"));
			capabilities.setCapability("appWaitPackage", properties.getProperty("package"));
			capabilities.setCapability("appActivity", properties.getProperty("appActivity"));
			capabilities.setCapability("appWaitActivity", properties.getProperty("appActivity"));
			capabilities.setCapability("automationName", properties.getProperty("automationName"));
		}/*
		if (cargar) {
			capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME,"android");
			capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "G3123");       
			capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
			capabilities.setCapability("appPackage","ar.gob.coronavirus");
			capabilities.setCapability("appWaitPackage", "ar.gob.coronavirus");
			capabilities.setCapability("appActivity", "com.globant.pasaportesanitario.flujos.inicio.InicioActivity");
			capabilities.setCapability("appWaitActivity", "com.globant.pasaportesanitario.flujos.inicio.InicioActivity");
			capabilities.setCapability("automationName", "UiAutomator1");
		}*/
	}
	
	public static AndroidDriver getDriver() {
		if (driver == null) {
		new Driver(true);}
		return driver;
	}
	
	
}
