package Steps;

import cucumber.api.Scenario;
import static org.junit.Assert.*;
import Actions.Driver;
import Pages.PaginaDireccion;
import Pages.PaginaIngreso;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import io.appium.java_client.android.AndroidDriver;

	
public class PrimerUsoSteps {

	AndroidDriver driver;
	PaginaDireccion direccion;
	public PrimerUsoSteps() {
		driver = Driver.getDriver();
	}
    

	@Then("^ingresamos el telefono y la direccion en \"(.*)\"$")
	public void ingresamos_el_telefono_y_la_direccion_en(String provincia) throws InterruptedException {
		direccion = new PaginaDireccion(driver);
		
		direccion.CargarDireccion(provincia);
		System.out.println("Se ingresa la provincia "+provincia);
	}

}
