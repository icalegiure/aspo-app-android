package Steps;

import static org.junit.Assert.*;

import org.openqa.selenium.interactions.Actions;

import Actions.Driver;
import Pages.PaginaAutodiagnostico;
import Pages.PaginaCertificado;
import Pages.PaginaCondicionPreex;
import Pages.PaginaCuestionario;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.And;
import io.appium.java_client.android.AndroidDriver;

public class AutoDiagnosticoSteps {

	AndroidDriver driver;
	PaginaCertificado certificado;
	
	public AutoDiagnosticoSteps() {
		driver = Driver.getDriver();
	}
	
	@When("^ingresamos nuevo autodianostico$")
	public void ingresamos_nuevo_autodianostico() {
		certificado = new PaginaCertificado(driver);
		certificado.nuevoAutodiagnostico.click();
	}
	
	@Then("^colocamos la temperatura del ciudadano$")
	public void colocamos_la_temperatura_del_ciudadano() throws InterruptedException {
		PaginaAutodiagnostico  autodiagnostico = new PaginaAutodiagnostico(driver);		
		autodiagnostico.completarTemperatura("37");
	}
	
	@Then("^colocamos la temperatura del ciudadano con fiebre$")
	public void colocamos_la_temperatura_del_ciudadano_con_fiebre() throws InterruptedException {
		PaginaAutodiagnostico  autodiagnostico = new PaginaAutodiagnostico(driver);		
		autodiagnostico.completarTemperatura("39");
	}
	
	@And("^contestamos el cuestionario a todo no$")
	public void contestamos_el_cuestionario_a_todo_no() {
		PaginaCuestionario cuestionario = new PaginaCuestionario(driver);
    	cuestionario.CompletarCuestionarioNO();
	}
	
	@And("^contestamos el cuestionario con peridida de olfato$")
	public void contestamos_el_cuestionario_olfato_si() {
		PaginaCuestionario cuestionario = new PaginaCuestionario(driver);
    	cuestionario.CompletarCuestionarioPerdidaDeOlfato();
	}
	
	@And("^colocamos que no tenemos enfermedades preexistente$")
	public void colocamos_que_no_tenemos_enfermedades_preexistente() {
		PaginaCondicionPreex condicion = new PaginaCondicionPreex(driver);
    	condicion.AvanzarSinEnfermedad();
	}
	
	@And("^avanzar sin enfermedades confirmando el telefono$")
	public void avanzar_sin_enfermedades_confirmando_el_telefono() {
		PaginaCondicionPreex condicion = new PaginaCondicionPreex(driver);
    	condicion.AvanzarConfirmandoTelefono();
	}
	
	@Then("^el autodianostico Ok se verifica el certificado$")
	public void el_autodianostico_Ok_se_verifica_el_certificado() {
		certificado = new PaginaCertificado(driver);
    	certificado.AceptarResultado();    	
    	boolean verificacion =  certificado.VerificarResultado("Sin");    	
	
		
    	assertTrue("No se visualiza el resultado de diagnostico OK",verificacion);
	}
	
	@Then("^el autodianostico Ok se verifica el certificado derivado a salud$")
	public void el_autodianostico_Ok_se_verifica_el_certificado_derivado_salud() {
		certificado = new PaginaCertificado(driver);
    	certificado.AceptarResultado();    	
    	boolean verificacion =  certificado.VerificarResultado("En");
    	assertTrue("No se visualiza el resultado de diagnostico OK",verificacion);
	}
	
}
