package Steps;
import static org.junit.Assert.*;

import Actions.Driver;
import Pages.PaginaCertificado;
import cucumber.api.java.en.Then;
import io.appium.java_client.android.AndroidDriver;

public class CertificadoSteps {
	AndroidDriver driver;
	PaginaCertificado certificado;
	
	public CertificadoSteps() {
		driver = Driver.getDriver();
	}
	
	@Then("^Se verifica el certificado de circulacion$")
	public void verifica_el_certificado_de_circulacion() throws InterruptedException {
		certificado = new PaginaCertificado(driver);		
		boolean verificacion = certificado.VerificarCertificadoDeCirculacion();
		assertTrue("No se visualiza el certificado de circulacion",verificacion);
		
	}
}
