package Steps;

import cucumber.api.Scenario;
import static org.junit.Assert.*;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import Actions.Driver;
import Pages.PaginaIngreso;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.After;
import io.appium.java_client.android.AndroidDriver;

public class LoginSteps {

	AndroidDriver driver;
	boolean verificacion;
	
	@After
	public void cerrar_aplicacion(Scenario scenario) {
		driver = Driver.getDriver();
		byte[] screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);		
		scenario.embed(screenshot, "image/png");
		driver.closeApp();
		System.out.println("Se cerro la app");
	}
	
	@Given("^que abrimos la aplicacion$")
	public void que_abrimos_la_aplicacion() {
		driver = Driver.getDriver();
		driver.launchApp();
	}

	@When("^ingresamos dni \"(.*)\" numero de tramite \"(.*)\" y genero \"(.*)\" y presionamos Ingresar$")
	public void ingresamos_dni_numero_de_tramite_y_genero_y_presionamos_Ingresar(String dni, String nroTramite, String genero) throws Exception {
		PaginaIngreso login = new PaginaIngreso(driver);
		System.out.println("0000"+genero.trim()+"0000");
		verificacion = login.IngresoDNI(dni,nroTramite, genero.trim().toUpperCase());
	}

	@Then("^Se loguea correctamente$")
	public void se_loguea_correctamente() {
		assertTrue(verificacion);
	}
	
}
