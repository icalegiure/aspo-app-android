package ejemplo;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Pages.PaginaAutodiagnostico;
import Pages.PaginaCondicionPreex;
import Pages.PaginaCuestionario;
import Pages.PaginaIngreso;
import Pages.PaginaCertificado;

import java.net.MalformedURLException;
import java.net.URL;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import junit.framework.Assert;

public class EjemploTes {
	
	
	
	//@SuppressWarnings("deprecation")
	//@Test
	public void pruebaTest() throws Exception{
		
		URL url = null;
		
		String u = "http://127.0.0.1:4723/wd/hub";
		 try {
	        	url = new URL(u);
	        }
	        catch (MalformedURLException e) {
	            e.printStackTrace();
	        }
	
		DesiredCapabilities appiumOptions = new DesiredCapabilities();
        appiumOptions.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
        appiumOptions.setCapability(MobileCapabilityType.DEVICE_NAME, "MotoE2");       
        appiumOptions.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, 60);
        appiumOptions.setCapability("appPackage", "ar.gob.coronavirus");
        appiumOptions.setCapability("appWaitPackage", "ar.gob.coronavirus");
        appiumOptions.setCapability("appActivity", "com.globant.pasaportesanitario.flujos.inicio.InicioActivity");
        appiumOptions.setCapability("appWaitActivity", "com.globant.pasaportesanitario.flujos.inicio.InicioActivity");
        appiumOptions.setCapability("automationName", "UiAutomator1");
        
       

    	AndroidDriver driver = new AndroidDriver (url,appiumOptions);  
    	
    	PaginaIngreso login = new PaginaIngreso(driver);
    	
		login.IngresoDNI("25193653","68197156", "F");
		
		PaginaCertificado certificado = new PaginaCertificado(driver);
		driver.scrollTo("NUEVO");
		certificado.nuevoAutodiagnostico.click();
		
		
		PaginaAutodiagnostico  autodiagnostico = new PaginaAutodiagnostico(driver);		
		autodiagnostico.completarTemperatura("37");

    	PaginaCuestionario cuestionario = new PaginaCuestionario(driver);
    	cuestionario.CompletarCuestionarioNO();
    	
    	PaginaCondicionPreex condicion = new PaginaCondicionPreex(driver);
    	condicion.AvanzarSinEnfermedad();
    	
    	driver.scrollTo("CERTIFICADO");
    	
    	Assert.assertFalse("No se visualiza el resultado de diagnostico OK", certificado.quedateEnCasa.getText().contains("Quedate en casa"));
    	
    	driver.quit();
    	    	
    	
    	
	}
}