package Pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PaginaDireccion extends PaginaBase {

	
	public PaginaDireccion(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	//Agrego 2 botones de pop up de si concedes mostrar tu localizacion
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[2]")
	public MobileElement permitirLocalizacion;
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.ScrollView/android.widget.LinearLayout/android.widget.Button[1]")
	public MobileElement cancelarLocalizacion;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btn_siguiente_confirmacion_datos_identificacion_fragment")
	public MobileElement confirmarDatos;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/tie_telefono_identificacion_fragment")
	public MobileElement telefono;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btn_siguiente_telefono_fragment")
	public MobileElement telefonoSiguiente;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/dropdown_provincia_identificacion_fragment")
	public MobileElement provincia;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/dropdown_localidades_identificacion_fragment")
	public MobileElement localidad;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/tie_calle_identificacion_fragment")
	public MobileElement calle;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/tie_codigo_postal_identificacion_fragment")
	public MobileElement codigoPosatal;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/tie_numero_casa_identificacion_fragment")
	public MobileElement numero;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btn_siguiente_direccion_completa_identificacion_fragment")
	public MobileElement finalizar;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btn_action_pantalla_completa_dialog")
	public MobileElement continuar;
	
	
	
	public void CargarDireccion(String prov) throws InterruptedException  {
		WebDriverWait wait = new WebDriverWait(driver, 5);
		try {
			
			wait.until(ExpectedConditions.visibilityOf(confirmarDatos));		    
		    confirmarDatos.click();
		}
		catch(Exception e) {
			//throw new Exception("Usuario ya registrado");
		}
		
	    
	    wait.until(ExpectedConditions.visibilityOf(telefono));
		sendKeysToElement("11234567", telefono, false);		
		telefonoSiguiente.click();		
		
		//agrego si pide localizacion que la cancele
		try {
			cancelarLocalizacion.click();
		}
		catch(Exception e) {
			//System.out.println("No se encontro el boton cancelar localizacion " + e.getMessage());
		}
		
		wait.until(ExpectedConditions.visibilityOf(provincia));		
		
		
		sendKeysToElement(prov,provincia,true);				
		
		localidad.sendKeys("");
		localidad.click();
		
		sendKeysToElement("San Martin",calle,false);
		
		sendKeysToElement("1878",codigoPosatal,false);
		
		sendKeysToElement("123",numero,false);
		
		driver.scrollTo("FINALIZAR");
		
		finalizar.click();
		
	    wait.until(ExpectedConditions.visibilityOf(continuar));
	    continuar.click();
	}
	
}
