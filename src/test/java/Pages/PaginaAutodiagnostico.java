package Pages;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PaginaAutodiagnostico extends PaginaBase {

	public PaginaAutodiagnostico(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/et_temperatura")
	public MobileElement temperatura;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btn_siguiente")
	public MobileElement suiguiente;


	public void completarTemperatura(String temp) throws InterruptedException {
		
		WebDriverWait wait = new WebDriverWait(driver, 40);
	    wait.until(ExpectedConditions.visibilityOf(temperatura));
	    	
		sendKeysToElement(temp, temperatura, false);		
		driver.scrollTo("SIGUIENTE");
		suiguiente.click();
	}
	
}
