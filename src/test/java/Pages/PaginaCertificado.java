package Pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.interactions.Actions;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PaginaCertificado extends PaginaBase{

	public PaginaCertificado(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btnAceptarResultado")
    public MobileElement aceptarResultado;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/imgMinSalud")
    public MobileElement imagenMinisterio;	
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/quedate_en_casa")
    public MobileElement quedateEnCasa;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/cantidad_horas")
    public MobileElement cantidadHoras;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/boton_nuevo_diagnostico")
    public MobileElement nuevoAutodiagnostico;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/desvincular_dni")
    public MobileElement desvincular;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/qr_status")
    public MobileElement certificadoDeCirculacion;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/sintoma_resultado")
    public MobileElement certificado;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/sintoma_resultado")
    public MobileElement resultado;
	
	@AndroidFindBy(id = "abrir")
    public MobileElement menu;
	
	
	
	
	public void AceptarResultado() {
		try {
			
			
			if (imagenMinisterio.isDisplayed()){
				driver.scrollTo("ACEPTAR");
				aceptarResultado.click();
			}
		}
		catch(NoSuchElementException  ex) {
			
		}
	}
	
	public boolean VerificarCertificadoDerivadoSalud() {
		boolean casa =  false;
		try {
		
			//driver.scrollTo("CERTIFICADO");
			casa = certificado.getText().contains("DERIVADO A SALUD");
		}
		catch(NoSuchElementException  ex) {
			return casa;
		}
		return casa;
	}
	
	public boolean VerificarCertificadoQuedateEnCasa() {
		boolean casa =  false;
		try {
		
			driver.scrollTo("CERTIFICADO");
			casa = quedateEnCasa.getText().contains("Quedate en casa");
		}
		catch(NoSuchElementException  ex) {
			return casa;
		}
		return casa;
	}
	
	public boolean VerificarResultado(String resul) {
		boolean casa =  false;
		try {
		
			casa = resultado.getText().contains(resul);
		}
		catch(NoSuchElementException  ex) {
			return casa;
		}
		return casa;
	}
	
	public boolean VerificarCertificadoDeCirculacion() {
		
		boolean ret = false;
		try {
			ret = certificadoDeCirculacion.isDisplayed();
		}
		catch (NoSuchElementException  ex) {
			return ret;
		}
		return ret;
	}
	
}
