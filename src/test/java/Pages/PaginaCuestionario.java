package Pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PaginaCuestionario extends PaginaBase{

	public PaginaCuestionario(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.widget.LinearLayout[2]/android.widget.RadioGroup/android.widget.RadioButton[1]")
    public MobileElement olfatoSi;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/radio_perdida_olfato_no")
    public MobileElement olfatoNo;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/radio_tos_si")
    public MobileElement tosSi;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/radio_tos_no")
    public MobileElement tosNo;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/radio_respiratoria_si")
    public MobileElement  respiratoriaSi;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/radio_respiratoria_no")
    public MobileElement respiratoriaNo;	
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/radio_perdida_gusto_si")
    public MobileElement gustoSi;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/radio_perdida_gusto_no")
	public MobileElement gustoNo;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btn_siguiente")
	public MobileElement suiguiente;
	
	public void  CompletarCuestionarioNO() {
		
		WebDriverWait wait = new WebDriverWait(driver, 40);
	    wait.until(ExpectedConditions.visibilityOf(olfatoSi));
	    		
		driver.scrollTo("garganta");		
		driver.scrollTo("SIGUIENTE");	
		suiguiente.click();
		
	}
	public void  CompletarCuestionarioPerdidaDeOlfato() {
		
		//WebDriverWait wait = new WebDriverWait(driver, 40);
	    //wait.until(ExpectedConditions.visibilityOf(olfatoNo));
	    
		olfatoSi.click();		
		
		driver.scrollTo("garganta");		
		driver.scrollTo("SIGUIENTE");		
		
		suiguiente.click();
		
	}
	
	

}
