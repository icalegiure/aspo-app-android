package Pages;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PaginaCondicionPreex extends PaginaBase{

	public PaginaCondicionPreex(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.widget.CheckBox[1]")
    public MobileElement convivenciaConfirmada;
	
	@AndroidFindBy(xpath = "/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.widget.FrameLayout/android.widget.FrameLayout/android.view.View/android.view.View/android.widget.CheckBox[2]")
    public MobileElement contactoConfirmado;
	
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/cb_embarazo")
    public MobileElement embarazo;	
	
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/cb_cancer")
    public MobileElement cancer;
	
	@AndroidFindBy(id = "	ar.gob.coronavirus:id/cb_diabetes")
    public MobileElement diabetes;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/cb_enfermedad_hepatica")
    public MobileElement enfermedadHepatica;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/cb_enfermedad_renal")
    public MobileElement enfermedadRenal;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/cb_enfermedad_respiratoria")
    public MobileElement enfermedadRespiratoria;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/cb_enfermedad_cardiaca")
    public MobileElement enfermedadCardiaca;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btn_siguiente")
    public MobileElement verResultado;
	
	@AndroidFindBy(id = "android:id/button1")
    public MobileElement enviarDeclaracion;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btn_siguiente")
    public MobileElement confirmar;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/continue_button")
    public MobileElement telefonoConfirmar;
	
	
	
	
	public void AvanzarSinEnfermedad() {
		
	   WebDriverWait wait = new WebDriverWait(driver, 40);	  
	    wait.until(ExpectedConditions.visibilityOf(contactoConfirmado));
		driver.scrollTo("SIGUIENTE");
		verResultado.click();
		wait.until(ExpectedConditions.visibilityOf(confirmar));
		confirmar.click();
		wait.until(ExpectedConditions.visibilityOf(enviarDeclaracion));			
		enviarDeclaracion.click();
		
	}
	
	public void AvanzarConfirmandoTelefono() {
		
		   WebDriverWait wait = new WebDriverWait(driver, 40);	  
		    wait.until(ExpectedConditions.visibilityOf(contactoConfirmado));
			driver.scrollTo("SIGUIENTE");
			verResultado.click();
			wait.until(ExpectedConditions.visibilityOf(confirmar));
			confirmar.click();
			wait.until(ExpectedConditions.visibilityOf(enviarDeclaracion));			
			enviarDeclaracion.click();
			wait.until(ExpectedConditions.visibilityOf(telefonoConfirmar));
			telefonoConfirmar.click();
		}
	
}
