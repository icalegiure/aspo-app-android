package Pages;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;

public class PaginaIngreso extends PaginaBase {

	private static final int KEYBOARD_ANIMATION_DELAY = 5;
	
	
	
	public PaginaIngreso(AppiumDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/et_numero_dni_identificacion_fragment")
    private MobileElement campoDni;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/et_numero_tramite_identificacion_fragment")
    private MobileElement nrotramite;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/rb_masculino_identificacion_fragment")
    private MobileElement masculino;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/rb_femenino_identificacion_fragment")
    private MobileElement femenino;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/btn_siguiente_dni_manual_identificacion_fragment")
    private MobileElement suguiente;
	
	@AndroidFindBy(id = "ar.gob.coronavirus:id/tv_error_message")
    private MobileElement errorUsuario;
	
	
	/**
	 * 
	 * @param usuario
	 * @param numerotramite
	 * @param genero = F o M
	 * @return
	 * @throws Exception 
	 */
	public boolean IngresoDNI(String usuario, String numerotramite, String genero) throws Exception {
       
        
        WebDriverWait wait = new WebDriverWait(driver, 40);
    	wait.until(ExpectedConditions.visibilityOf(campoDni));
    	
    	boolean estatus = sendKeysToElement(usuario, campoDni, false);
        
        //Thread.sleep(KEYBOARD_ANIMATION_DELAY);
    	sendKeysToElement(numerotramite,nrotramite,false);
        
    	if (genero.equals("F")) {
        	femenino.click();
        }else {
        	masculino.click();
        }
    	driver.scrollTo("SIGUIENTE");
        suguiente.click();

        Thread.sleep(5000);
        try {
        	if (errorUsuario.isDisplayed()){
        		System.out.println("Error de datos - al registrar el dni ");
            	estatus = false;
            }
        }
        catch(NoSuchElementException  ex)
        {}
        System.out.println("Ciuadano ingresa correctamente");
        return estatus;
    }
	 
}
